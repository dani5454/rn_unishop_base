import React from 'react';
import { Provider } from 'react-redux';
import AppRouteConfigs from './src/navigation/Navigator';
import store from './src/redux/store';

console.disableYellowBox = true;

export default function App() {
    return (
        <Provider store={store}>
            <AppRouteConfigs />
        </Provider>
    );
}
