import React from 'react';
import {Image} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {enableScreens} from 'react-native-screens';
import {navigationRef} from './../utilMethods/RootNavigation';
import {Width} from '../utilMethods/Utils';
import DrawerContent from '../components/Drawer';
// SCREENS
import Splash from '../views/Splash';
import OnBoard from '../views/OnBoard';
import Login from '../views/Login';
import Register from '../views/Register';
import HomeScreen from '../views/HomeScreen';
import Store from '../views/Store';
import DeliverySchedule from '../views/DeliverSchedule';
import Settings from '../views/Settings';
import ProductList from '../views/ProductList';
import UpdateUserProfile from '../views/UpdateUserProfile';
import MapView from '../views/MapView';
import OnGoingDrivers from '../views/OnGoingDrivers';

enableScreens();

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const HomeTabNavigator = ({navigation, route}) => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#FF6F00',
        inactiveTintColor: '#000000',
        showLabel: false,
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          let iconName;
          if (route.name === 'Store') {
            iconName = require('../assets/images/store.png');
          } else if (route.name === 'DeliverySchedule') {
            iconName = require('../assets/images/deliverySchedule.png');
          } else if (route.name === 'Settings') {
            iconName = require('../assets/images/Settings.png');
          }
          return (
            <Image
              source={iconName}
              style={{
                width: Width('12%'),
                height: Width('12%'),
                tintColor: color,
              }}
            />
          );
        },
      })}>
      <Tab.Screen name="Store" component={Store} />
      <Tab.Screen name="DeliverySchedule" component={DeliverySchedule} />
      <Tab.Screen name="Settings" component={Settings} />
    </Tab.Navigator>
  );
};

const HomeDrawerNavigator = ({navigation, route}) => {
  return (
    <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="Home" children={HomeTabNavigator} />
    </Drawer.Navigator>
  );
};

function AppRouteConfigs() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OnBoard"
          component={OnBoard}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ProductList"
          component={ProductList}
          // options={{headerShown: false}}
        />
        <Stack.Screen
          name="UpdateUserProfile"
          component={UpdateUserProfile}
          // options={{headerShown: false}}
        />
        <Stack.Screen
          name="ListDrivers"
          component={OnGoingDrivers}
          // options={{headerShown: false}}
        />
        <Stack.Screen
         name="MapView"
         component={MapView}
         // options={{headerShown: false}}
        />
        <Stack.Screen
          name="HomeScreen"
          children={HomeDrawerNavigator}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppRouteConfigs;
