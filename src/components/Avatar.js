import React from 'react';
import {View, Image} from 'react-native';

const Avatar = ({dim, image}) => (
  <View
    style={{
      width: dim,
      height: dim,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'transparent',
    }}>
    <Image
      source={image}
      style={{
        width: dim - 30,
        height: dim - 30,
        borderRadius: (dim - 30) / 2,
      }}
      // onError={e => console.log('OnError---->>>', e)}
    />
  </View>
);

export default Avatar;
