import React from 'react';
import {ActivityIndicator, Modal, View} from "react-native";

class Loader {

    renderLoading(visible) {
        return (<View>{visible ?
                <Modal transparent={true} onRequestClose={() => null} visible={visible}>
                    <View style={{flex: 1,backgroundColor: '#00000070', alignItems: 'center', justifyContent: 'center'}}>
                        <ActivityIndicator size="large" color='#030031'/>
                    </View>
                </Modal> : null}</View>

        );
    }
}

export default new Loader();
