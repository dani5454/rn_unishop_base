import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  TouchableWithoutFeedback,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {Width, Height} from '../utilMethods/Utils';
import Fonts from '../utilMethods/FontFamily';
import Avatar from './Avatar';

const Card = ({
  type,
  index,
  image,
  orderNo,
  clientName,
  Direction,
  accept,
  reject,
  dispatch,
  cardOnPress,
}) => (
  <ImageBackground
    source={require('../assets/images/notification.png')}
    style={styles.cardContainer}
    resizeMode={'cover'}>
    <TouchableOpacity
        activeOpacity={0.8}
        onPress={()=> cardOnPress()}
        style={styles.container}>
      <View style={{flex: 1, flexDirection: 'row', marginTop: Height('2%')}}>
        <Avatar
          image={{uri: image}} //require('./../assets/images/avatar-512.png')
          dim={Width('22%')}
        />
        <View
          style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
          <Text style={Fonts.gothamRounded_light}>Orden # NUI-{index}</Text>
          <Text style={Fonts.gothamRounded_light}>Cliente: {clientName}</Text>
          <Text style={Fonts.gothamRounded_light}>Direccion: {Direction}</Text>
          <View style={{flexDirection: 'row', marginTop: Width('3%')}}>
            {type === 'selection' ? (
              <View style={{flexDirection: 'row'}}>
                <TouchableWithoutFeedback onPress={() => accept()}>
                  <View style={[styles.button, {backgroundColor: '#6c9f27'}]}>
                    <Text
                      style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>
                      Acceptar
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => reject()}>
                  <View
                    style={[
                      styles.button,
                      {marginLeft: Width('4%'), backgroundColor: '#D64B12'},
                    ]}>
                    <Text
                      style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>
                      Rechazar
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            ) : (
              <View style={{flexDirection: 'row'}}>
                <TouchableWithoutFeedback onPress={() => dispatch()}>
                  <View
                    style={[
                      styles.button,
                      {
                        width: Width('50%'),
                        height: Height('4%'),
                        backgroundColor: '#6c9f27',
                      },
                    ]}>
                    <Text
                      style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>
                      A entregar
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            )}
          </View>
        </View>
      </View>
    </TouchableOpacity>
  </ImageBackground>
);

export default Card;

const styles = StyleSheet.create({
  cardContainer: {
    height: Height('23%'),
    width: Width('100%'),
  },
  container: {
    flex: 1,
    marginTop: Height('5%'),
    marginBottom: Height('1.5%'),
    marginLeft: Width('5%'),
    marginRight: Width('5%'),
  },
  button: {
    height: Height('3%'),
    width: Width('23%'),
    borderRadius: Width('1.2%'),
    alignItems: 'center',
    justifyContent: 'center',
  },
});
