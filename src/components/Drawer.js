import React from 'react';
import {
  Image,
  TouchableOpacity,
  Alert,
  TouchableWithoutFeedback,
  SafeAreaView,
  Text,
  View,
  StyleSheet,
} from 'react-native';
// import {StackActions} from '@react-navigation/native';
import {Width, Height} from '../utilMethods/Utils';
import firebase from 'react-native-firebase';
import Fonts from '../utilMethods/FontFamily';
import {connect} from 'react-redux';

class DrawerContent extends React.Component {
  constructor(props) {
    super(props);
  }

  logout = () => {
    this.props.navigation.closeDrawer();
    Alert.alert(
      'cerrar sesión',
      '¿Estás seguro de que quieres cerrar sesión?',
      [
        {
          text: 'CANCELAR',
          onPress: () => console.log('cancel'),
          style: 'destructive',
        },
        {text: 'OK', onPress: () => this.signOut()},
      ],
      {cancelable: false},
    );
  };

  signOut = () => {
    firebase.auth().signOut();
    this.props.navigation.replace('OnBoard');
  };

  selectMapView = () => {
    if (this.props.userProfile.shopper) {
      this.props.navigation.navigate('ListDrivers');
    } else {
      this.props.navigation.navigate('MapView', {shopper: false});
    }
  };

  render() {
    const {userProfile} = this.props;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#6c9f27'}}>
        <View style={{alignItems: 'center'}}>
          <Image
            source={require('../assets/images/notification_white.png')}
            style={{width: Width('15%'), height: Width('15%')}}
          />
        </View>
        <View
          style={{marginLeft: Width('3%'), marginRight: Width('3%'), flex: 1}}>
          <TouchableWithoutFeedback
            onPress={() => this.props.navigation.navigate('UpdateUserProfile')}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style={{flexDirection: 'column'}}>
                <Text style={styles.colors}>
                  {userProfile && userProfile.name}
                </Text>
                <Text style={styles.colors}>
                  {userProfile && userProfile.email}
                </Text>
                <Text style={styles.colors}>
                  {userProfile && userProfile.phone}
                </Text>
              </View>
              <View style={{flexDirection: 'column', alignItems: 'center'}}>
                <View style={styles.profilePhoto} />
                <Text style={styles.colors}>
                  {userProfile && userProfile.employeeId}
                </Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
          <View style={{borderWidth: 2, borderColor: 'white'}} />
          <View style={{flex: 1}}>
            <TouchableOpacity
              style={{marginTop: Height('2.4%')}}
              onPress={() => this.props.navigation.navigate('Store')}>
              <Text style={[styles.colors, Fonts.gothamRounded_bold]}>
                ORDENES ACEPTADAS
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{marginTop: Height('2.4%')}}
              onPress={() => this.props.navigation.navigate('DeliverySchedule')}>
              <Text style={[styles.colors, Fonts.gothamRounded_bold]}>
                ORDENES ENTREGADAS
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{marginTop: Height('2.4%')}}
              onPress={this.selectMapView}>
              <Text style={[styles.colors, Fonts.gothamRounded_bold]}>
                RASTREAR ORDEN
              </Text>
            </TouchableOpacity>
            <View style={{flex: 1, justifyContent: 'flex-end'}}>
              <TouchableOpacity style={{}} onPress={() => console.log('AYUDA')}>
                <Text style={[styles.colors, Fonts.gothamRounded_bold]}>
                  AYUDA
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            marginTop: Height('2.2%'),
            backgroundColor: 'lightgrey',
          }}>
          <TouchableWithoutFeedback onPress={() => this.logout()}>
            <View style={styles.logout}>
              <Image
                source={require('../assets/images/logout-512.png')}
                style={{
                  height: 20,
                  opacity: 0.5,
                  width: 20,
                  marginLeft: Width('5%'),
                }}
                resizeMode={'cover'}
              />
              <Text
                style={[Fonts.gothamRounded_bold, {marginLeft: Width('8%')}]}>
                Cerrar sesión
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  colors: {
    color: 'white',
  },
  profilePhoto: {
    borderRadius: 50,
    height: 45,
    width: 45,
    backgroundColor: '#fff',
  },
  logout: {
    height: Height('5'),
    borderBottomWidth: 0.3,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

const mapStateToProps = state => {
  return {
    userProfile: state.login.userProfile,
  };
};

export default connect(
  mapStateToProps,
  null,
)(DrawerContent);
