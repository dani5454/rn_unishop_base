
const Fonts = {
    gothamRounded_bold: {
        fontFamily: 'GOTHAMROUNDED-BOLD',
        fontSize: 14,
    },
    gothamRounded_light: {
        fontFamily: 'GOTHAMROUNDED-LIGHT',
        fontSize: 14,
    },
    gothamRounded_book: {
        fontFamily: 'GOTHAMROUNDEDBOOK',
        fontSize: 14,
    }
};

export default Fonts;
