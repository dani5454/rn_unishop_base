import * as order_constant from './../constants/orders.constant';

export default function ordersReducer(
  state = {
    fetching: false,
    userOrders: [],
    dispatchOrders: [],
  },
  action,
) {
  switch (action.type) {
    case order_constant.DO_ORDER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
      });
    case order_constant.FETCH_ORDER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
      });
    case order_constant.FETCH_ORDER_SUCCESS:
      return Object.assign({}, state, {
        fetching: false,
        userOrders: action.payload,
        dispatchOrders: action.dispatch,
      });
    case order_constant.UPDATE_ORDER_SUCCESS:
      return Object.assign({}, state, {
        fetching: false,
        userOrders: action.payload,
        dispatchOrders: action.dispatch,
      });
    default:
      return state;
  }
}
