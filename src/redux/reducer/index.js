import {combineReducers} from 'redux';
import userReducer from './userReducer';
import ordersReducer from './OrderReducer';

const rootReducer = combineReducers({
  login: userReducer,
  orders: ordersReducer,
});

export default rootReducer;
