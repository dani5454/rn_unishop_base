import * as user_constant from './../constants/user.constant';

export default function userReducer(
  state = {
    isLoader: false,
    loginStatus: 'uninitiated',
    fetchProfile: 'uninitiated',
    userProfile: {},
  },
  action,
) {
  switch (action.type) {
    case user_constant.DO_LOGIN:
      return Object.assign({}, state, {
        loginStatus: 'initiated',
      });
    case user_constant.FETCH_USER_DATA:
      return Object.assign({}, state, {
        isLoader: true,
      });
    case user_constant.FETCH_USER_SUCCESS:
      return Object.assign({}, state, {
        isLoader: false,
        userProfile: action.payload,
      });
    case user_constant.FETCH_USER_FAIL:
      return Object.assign({}, state, {
        isLoader: false,
      });
    case user_constant.FETCHING_PROFILE:
      return Object.assign({}, state, {
        fetchProfile: 'initiated',
      });
    default:
      return state;
  }
}
