import {call, put, takeEvery, takeLatest, delay} from 'redux-saga/effects';
import * as user_constants from '../constants/user.constant';
import * as RootNavigation from '../../utilMethods/RootNavigation';
import firebase from 'react-native-firebase';
import {Alert} from 'react-native';

export function* userLogin() {
  yield takeLatest(user_constants.DO_LOGIN, function*(action) {
    yield put({type: user_constants.FETCH_USER_DATA});
    try {
      const loginData = yield call(
        attemptLogin,
        action.payload.email,
        action.payload.password,
      );
      if (loginData.hasError) {
        yield put({type: user_constants.FETCH_USER_FAIL});
        yield delay(500);
        yield call(showErrorAlert, loginData.errorMessage);
      } else {
        const uid = firebase.auth().currentUser.uid;
        const fetchUser = yield call(fetchUserProfile, uid);
        if (!fetchUser.hasError) {
          if (fetchUser.data.driver) {
            yield put({
              type: user_constants.FETCH_USER_SUCCESS,
              payload: fetchUser.data,
            });
            yield delay(400);
            RootNavigation.reset('HomeScreen');
          } else {
            yield call(signOut);
            yield put({type: user_constants.FETCH_USER_FAIL});
            yield delay(500);
            yield call(
              showErrorAlert,
              'El usuario no se registra como conductor',
            );
          }
        } else {
          yield call(signOut);
          yield put({type: user_constants.FETCH_USER_FAIL});
          yield delay(500);
          yield call(
            showErrorAlert,
            'Error de servidor interno.\nInténtalo de nuevo',
          );
        }
      }
    } catch (error) {
      yield put({type: user_constants.FETCH_USER_FAIL});
      yield call(signOut);
      yield delay(500);
      yield call(
        showErrorAlert,
        'Error de servidor interno.\nInténtalo de nuevo',
      );
    }
  });
}

export function* userSignUp() {
  yield takeEvery(user_constants.DO_REGISTER, function*(action) {
    yield put({type: user_constants.FETCH_USER_DATA});
    try {
      const signUpData = yield call(
          attemptSigUp,
          action.payload.email,
          action.payload.password,
      );
      if (signUpData.hasError) {
        yield put({type: user_constants.FETCH_USER_FAIL});
        // yield delay(500);
        Alert.alert('Error del Servidor', signUpData.errorMessage);
        yield put({type: user_constants.FETCH_USER_FAIL});
      } else {
        console.log('payload----->>>>', action.payload.type);
        let type = action.payload.type === 'Comprador';
        console.log('type----->>>>', type);
        const setProfile = yield call(
          setUpUserProfile,
          action.payload.email,
          action.payload.firstName,
          action.payload.lastName,
          action.payload.phone,
          action.payload.address,
          action.payload.driverId,
          type,
        );
        if (!setProfile.hasError) {
          const profile = {
            address: action.payload.address,
            email: action.payload.email,
            phone: action.payload.phone,
            lastName: action.payload.lastName,
            name: action.payload.name,
            employeeId: action.payload.driverId,
            customer: false,
            shopper: type,
            driver: true,
            logo: '',
            photo: ''
          };
          yield put({
            type: user_constants.FETCH_USER_SUCCESS,
            payload: profile,
          });
          yield delay(400);
          RootNavigation.navigate('HomeScreen');
        } else {
          yield call(signOut);
          yield put({type: user_constants.FETCH_USER_FAIL});
          yield delay(500);
          yield call(
            showErrorAlert,
              'Algo salió mal. Por favor, inténtelo de nuevo más tarde',
          );
        }
      }
    } catch (error) {
      yield put({type: user_constants.FETCH_USER_FAIL});
      yield call(
          showErrorAlert,
          'Algo salió mal. Por favor, inténtelo de nuevo más tarde',
      );
    }
  });
}

export function* fetchUserData() {
  yield takeLatest(user_constants.FETCHING_PROFILE, function*(action) {
    try {
      const userProfile = yield call(fetchUserProfile, action.payload.uid);
      if (!userProfile.hasError) {
        yield put({
          type: user_constants.FETCH_USER_SUCCESS,
          payload: userProfile.data,
        });
        yield delay(5500);
        RootNavigation.reset('HomeScreen');
      } else {
        yield delay(5500);
        RootNavigation.reset('OnBoard');
      }
    } catch (error) {
      console.log('ERROR---->>>', error);
    }
  });
}

export function* fetchUserOrders() {
  // yield takeLatest(user_constants.FETCHING_ORDERS, function*(action) {
  //   try {
  //     const userProfile = yield call(fetchOrders);
  //     if (!userProfile.hasError) {
  //       yield put({
  //         type: user_constants.FETCH_ORDERS_SUCCESS,
  //         payload: userProfile.data,
  //       });
  //     } else {
  //       yield put({type: user_constants.FETCH_ORDERS_FAIL});
  //     }
  //   } catch (error) {
  //     console.log('ERROR---->>>', error);
  //   }
  // });
}

const attemptLogin = (email, password) => {
  return firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(currentUser => {
      return {hasError: false, data: currentUser.user._user};
    })
    .catch(error => {
      return {hasError: true, errorMessage: error.message};
    });
};

const attemptSigUp = async (email, password) => {
  return await firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then(currentUser => {
      return {error: false};
    })
    .catch(error => {
      let message = error.code === 'auth/email-already-in-use' ? 'El correo electrónico ya existe. por favor intente con otro correo': error.message;
      return {error: true, errorMessage: message};
    });
};

const setUpUserProfile = (email, name, lastName, phone, address, driverId, shopper) => {
  return firebase
    .firestore()
    .collection('Profile')
    .doc(firebase.auth().currentUser.uid)
    .set({
      email: email,
      name: name,
      lastName: lastName,
      phone: phone,
      logo: '',
      photo: '',
      address: address,
      employeeId: 'Nui-' + driverId,
      customer: false,
      shopper: shopper,
      driver: true,
    })
    .then(() => {
      return {hasError: false};
    })
    .catch(err => {
      return {hasError: true};
    });
};

const fetchUserProfile = uid => {
  return firebase
    .firestore()
    .collection('Profile')
    .doc(uid)
    .get()
    .then(snapshot => {
      return {hasError: false, data: snapshot.data()};
    })
    .catch(error => {
      return {hasError: true};
    });
};

const fetchOrders = () => {
  return firebase
    .firestore()
    .collection('OrderQueue')
    .get()
    .then(snapshot => {
      return {error: false, data: snapshot._docs};
    })
    .catch(error => {
      return {error: true, errorMessage: error.message};
    });
};

const signOut = () => {
  firebase.auth().signOut();
  return true;
};

const showErrorAlert = errorMessage => {
  Alert.alert('Error del Servidor', errorMessage);
  return false;
};
