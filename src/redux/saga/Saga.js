import {all} from 'redux-saga/effects';
import {userLogin, userSignUp, fetchUserData} from './user.actions.api';
import {fetchOrders, updateOrders} from './order.actions.api';

const rootSaga = function* rootSaga() {
  yield all([
    userLogin(),
    userSignUp(),
    fetchUserData(),
    fetchOrders(),
    updateOrders(),
  ]);
};

export default rootSaga;
