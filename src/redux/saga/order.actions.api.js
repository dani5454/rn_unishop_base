import {call, put, takeEvery, takeLatest, delay} from 'redux-saga/effects';
import * as order_constants from '../constants/orders.constant';
import firebase from 'react-native-firebase';
import {Alert} from 'react-native';

export function* fetchOrders() {
  yield takeLatest(order_constants.DO_ORDER_FETCH, function*(action) {
    try {
      const fetchOrders = yield call(fetchOrder);
      const acceptedOrder = [];
      const dispatchOrder = [];
      if (!fetchOrders.hasError) {
        fetchOrders.data.filter(item => {
          if (item.data().userDetail !== '') {
            if (item.data().status === 'dispatched') {
              acceptedOrder.push(item.data());
            } else if (item.data().status === 'driver_accepted') {
              dispatchOrder.push(item.data());
            }
          }
        });
        yield delay(500);
        yield put({
          type: order_constants.FETCH_ORDER_SUCCESS,
          payload: acceptedOrder,
          dispatch: dispatchOrder,
        });
      } else {
        yield put({type: order_constants.FETCH_ORDER_FAIL});
        yield call(
          showErrorAlert,
          'Error de servidor interno.\nInténtalo de nuevo',
        );
      }
    } catch (error) {
      yield call(
        showErrorAlert,
        'Error de servidor interno.\nInténtalo de nuevo',
      );
    }
  });
}

export function* updateOrders() {
  yield takeLatest(order_constants.UPDATE_ORDER, function*(action) {
    const status = 'driver_accepted';
    const updateOrders = yield call(
      updateOrder,
      action.payload.uid,
      action.payload.orders,
      status,
    );
    if (!updateOrders.hasError) {
      let orders = action.payload.orders;
      let userOrders = action.payload.userOrders;
      let dispatchOrders = action.payload.dispatchOrders;
      for (let i = 0; i < userOrders.length; i++) {
        if (userOrders[i].orderId === orders.orderId) {
          userOrders.splice(i, 1);
        }
      }
      yield delay(300);
      let arr = [];
      orders.status = status;
      if (dispatchOrders.length === 0) {
        arr.push(orders);
        yield put({
          type: order_constants.UPDATE_ORDER_SUCCESS,
          payload: userOrders,
          dispatch: arr,
        });
      } else {
        for (let i = 0; i < dispatchOrders.length; i++) {
          arr.push(dispatchOrders[i]);
        }
        arr.push(order);
        yield delay(500);
        yield put({
          type: order_constants.UPDATE_ORDER_SUCCESS,
          payload: userOrders,
          dispatch: arr,
        });
      }
    } else {
      yield call(showErrorAlert, updateOrders.errorMessage);
    }
  });
}

const fetchOrder = () => {
  return firebase
    .firestore()
    .collection('OrderQueue')
    .get()
    .then(snapshot => {
      return {hasError: false, data: snapshot._docs};
    })
    .catch(error => {
      return {hasError: true, errorMessage: error.message};
    });
};

const updateOrder = (uid, order, status) => {
  return firebase
    .firestore()
    .collection('OrderQueue')
    .doc(uid)
    .set({
      userDetail: order.userDetail,
      orderId: order.orderId,
      orderTime: order.orderTime,
      userOrder: order.userOrder,
      status: status,
      payPalResponse: order.payPalResponse,
      userUID: order.userUID,
    })
    .then(() => {
      return {hasError: false};
    })
    .catch(error => {
      return {hasError: true, errorMessage: error.message};
    });
};

const showErrorAlert = errorMessage => {
  Alert.alert('Error del Servidor', errorMessage);
  return true;
};
