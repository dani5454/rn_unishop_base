import * as actions from './../constants/user.constant';

export function fetchUserProfile(uid) {
  return {type: actions.FETCHING_PROFILE, payload: uid};
}

export function userLogin(userData) {
  return {type: actions.DO_LOGIN, payload: userData};
}

export function registerUser(userData) {
  return {type: actions.DO_REGISTER, payload: userData};
}
