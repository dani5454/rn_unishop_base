import * as actions from './../constants/orders.constant';

export function fetchOrders() {
  return {type: actions.DO_ORDER_FETCH};
}

export function updateOrders(payload) {
  return {type: actions.UPDATE_ORDER, payload: payload};
}
