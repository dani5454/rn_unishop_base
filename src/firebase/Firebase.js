import firebase from 'react-native-firebase';

class Firebase {
    signIn = (email, password, callback) => {
        return firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then(currentUser => {
                return callback({error: false, response: currentUser.user});
            })
            .catch(error => {
                let message = error.code === 'auth/email-already-in-use' ? 'El correo electrónico ya existe. por favor intente con otro correo': error.message;
                return callback({error: true, errorMessage: message});
            });
    };

    signUp = (email, password, callback) => {
        return firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then(currentUser => {
                return callback({error: false, response: currentUser.user});
            })
            .catch(error => {
                return callback({error: true, errorMessage: error.message});
            });
    };

    setUpUserProfile = (uid, email, name, lastName, phone, address,logo, image,minPrice, callback) => {
        return firebase
            .firestore()
            .collection("Profile")
            .doc(uid)
            .set({
                email: email,
                name: name,
                lastName: lastName,
                phone: phone,
                minimumPrice: minPrice,
                logo: logo,
                photo: image,
                address: address,
                customer: false,
                driver: false,
            }).then(() => callback({error: false, message: 'Successfully written'}))
            .catch(err => {
                return callback({error: true, message: 'Internal Server Error'})
            })
    };

    updateUserProfile = (user,callback) => {
        return firebase
            .firestore()
            .collection("Profile")
            .doc(firebase.auth().currentUser.uid)
            .update({
                email: user.email,
                name: user.name,
                lastName: user.pegregal,
                phone: user.phone,
                address: user.address,
                shopper: user.shopper,
                employeeId: user.employeeId,
                logo: '',
                photo: '',
                customer: false,
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, errorMessage:'\'Error de servidor interno.\\nInténtalo de nuevo\''});
            });
    };

    getImageName = (length) => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    };

    uploadImage = async (imagePath, callback) => {
        let uid = firebase.auth().currentUser.uid;
        let imageName = await this.getImageName(10);
        const ref = firebase.storage().ref(`userImage/${uid}/${imageName}.png`);
        const uploadTask = ref.putFile(imagePath);

        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, (snapshot) => {
            if(firebase.storage.TaskState.SUCCESS) {
                console.log('Upload is complete');
            } else if (firebase.storage.TaskState.RUNNING) {
                console.log('Upload is running');
            }
        }, (error) => {
            callback({error: true, errorMessage:'Internal Server Error,\nPlease try again'});
        }, () => {
            uploadTask.then((resp) => {
                return callback({ error: false, url:resp.downloadURL });
            });
        });
    };

    initializeMarket = (uid, marketName, callback) => {
        return firebase
            .firestore()
            .collection("ProductList")
            .doc(uid)
            .set({
                brands: '',
                productsList: '',
                shop: marketName,
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, errorMessage:error.message});
            });
    };

    fetchMarketProfile = (callback) => {
        return firebase
            .firestore()
            .collection("Profile")
            .doc(firebase.auth().currentUser.uid)
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot.data()});
            }).catch((error) => {
                return callback({error: true, errorMessage:'Internal Server Error,\nPlease try again'});
            });
    };

    fetchOrders = (callback) => {
        return firebase
            .firestore()
            .collection("OrderQueue")
            // .limit(10)
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot._docs});
            }).catch((error) => {
                return callback({error: true, errorMessage:'Internal Server Error,\nPlease try again'});
            });
    };

    fetchOnGoingOrder = (callback) => {
        return firebase
            .firestore()
            .collection("OrderQueue")
            .where("status", "==", "driver_on_way")
            // .limit(10)
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot._docs});
            }).catch((error) => {
                return callback({error: true, errorMessage:'Internal Server Error,\nPlease try again'});
            });
    };

    fetchDriverOrder = async (callback) => {
        let query = await firebase
            .firestore()
            .collection("OrderQueue");
        let sub_query = query.where("status", "==", "driver_on_way");
        return sub_query.where("driver", "==", firebase.auth().currentUser.uid)
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot._docs});
            }).catch((error) => {
                return callback({error: true, errorMessage:'Internal Server Error,\nPlease try again'});
            });
    };

    updateOrderStatus = (order, driver, callback) => {
        return firebase
            .firestore()
            .collection("OrderQueue")
            .doc(order.userUID)
            .set({
                userDetail: order.userDetail,
                userOrder: order.userOrder,
                orderTime: order.orderTime,
                status: order.status,
                orderId: order.orderId,
                payPalResponse: order.payPalResponse,
                userUID: order.userUID,
                driver: driver,
                shopper: order.shopper,
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, errorMessage:error.message});
            });
    };

    fetchProducts = (callback) => {
        return firebase
            .firestore()
            .collection("ProductList")
            .doc(firebase.auth().currentUser.uid)
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot.data()});
            }).catch((error) => {
                return callback({error: true, errorMessage:'Error de servidor interno.\nInténtalo de nuevo'});
            });
    };

    updateMarketStatus = (products, callback) => {
        return firebase
            .firestore()
            .collection("ProductList")
            .doc(firebase.auth().currentUser.uid)
            .update({
                productsList: products,
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, errorMessage:'Error de servidor interno.\nInténtalo de nuevo'});
            });
    };

    updateMarketBrand = (brands, callback) => {
        return firebase
            .firestore()
            .collection("ProductList")
            .doc(firebase.auth().currentUser.uid)
            .update({
                brands: brands,
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, errorMessage:'Error de servidor interno.\nInténtalo de nuevo'});
            });
    };

    signOut = async () => {
        await firebase.auth().signOut().done();
    }
}

const firebaseModal = new Firebase();
export default firebaseModal;
