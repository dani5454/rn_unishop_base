import React from 'react';
import {
  View,
  ImageBackground,
  Alert,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  SafeAreaView,
} from 'react-native';
import {connect} from 'react-redux';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      changeState: false,
    };
  }

  componentDidMount() {}

  buttonPressed = () => {
    this.setState({changeState: true});
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text>{JSON.stringify(this.props.userProfile)}</Text>

        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity onPress={() => this.buttonPressed()}>
            <Text>Press Me</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapDispatchToProps = dispatch => {
  return {
    fetchProfile: uid => dispatch(fetchUserProfile(uid)),
  };
};

const mapStateToProps = state => {
  return {
    userProfile: state.login.userProfile,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeScreen);
