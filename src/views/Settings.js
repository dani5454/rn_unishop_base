import React from 'react';
import {View, Image, Linking,StatusBar,TouchableOpacity, StyleSheet} from 'react-native';
import {Height, Width} from '../utilMethods/Utils';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? Height('6%') : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
      <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class Setting extends React.Component {

  constructor(props) {
    super(props);
  }

  onPressPress = () => {
    console.log('Phone Press');
    let phoneNumber = '+923001234567';
    Linking.openURL(`tel:${phoneNumber}`)
  };

  render() {
    return (
        <View style={styles.container}>
          <MyStatusBar backgroundColor="#6c9f27" barStyle="light-content" />
          <View style={styles.header}>
            <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
            >
              <Image source={require('./../assets/images/Settings.png')}
                     style={styles.setting}
                     resizeMode={'cover'}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
              onPress={() => this.onPressPress()}
              style={styles.floatingBtn}>
            <Image source={require('./../assets/images/phone.png')}
                   style={{width: 20, height: 20, tintColor: '#FFFFFF'}}
                   resizeMode={'cover'}
            />
          </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  appBar: {
    backgroundColor:'#79B45D',
    height: APPBAR_HEIGHT,
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  header: {
    height: Height('6'),
    backgroundColor: '#6c9f27',
    flexDirection: 'row',
    alignItems: 'center',
  },
  setting: {
    height: Width('12'),
    width: Width('12'),
    marginLeft: Width('5'),
    tintColor: '#FFF'
  },
  floatingBtn: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#2da5fa',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 10,
    right: 10,
    zIndex: 99999999
  }
});
