import React from 'react';
import {Dimensions, StyleSheet, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
import Maps, {PROVIDER_GOOGLE, Polyline, Marker} from 'react-native-maps';
// import PolyLine from '@mapbox/polyline';
// import MAP_JSON from './../assets/Map_json';
import { getLocation } from './../utilMethods/geolocation-service';
import Geolocation from 'react-native-geolocation-service';
import firebaseModal from '../firebase/Firebase';
import MapViewDirections from 'react-native-maps-directions';
// import firebase from 'react-native-firebase';

const { width, height } = Dimensions.get('window');
const LATITUDE = 31.520370;
const LONGITUDE = 74.358747;
const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const GOOGLE_MAPS_API_KEY = 'AIzaSyDpzJA3pGJRA4Tt9oCX3bnDw3VOOqoBcHw';

class MapView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sourceData: [],
            destinationData: [],
            sourcePredictions: [],
            destinationPredictions: [],
            distanceData: {},
            latitude: 0,
            longitude: 0,
            coordinates: [
                {
                    latitude: 31.520370,
                    longitude: 74.358747,
                },
                {
                    latitude: 31.420370,
                    longitude: 74.358747,
                },
            ],
            coordinate: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
            },
        }
    }

    componentDidMount() {
        // TODO => driver_on_way
        // new firebase.firestore().GeoPoint('','');
        if(!this.props.userProfile.shopper) {
            firebaseModal.fetchDriverOrder((resp) => {
                if(!resp.error) {
                    let result = resp.data[0].data();
                    let coords = [
                        {
                            latitude: 31.520370,//response.coords.latitude
                            longitude: 74.358747,//response.coords.longitude
                        },
                        {
                            latitude: parseFloat(result.userDetail.address[0].lat),
                            longitude: parseFloat(result.userDetail.address[0].lng),
                        }
                    ];

                    this.setState({coordinates: coords});
                }
            }).done();
        } else {
            getLocation().then((response) => {
                let dest = this.props.route.params.detail;
                let coords = [
                    {
                        latitude: 31.520370,//response.coords.latitude
                        longitude: 74.358747,//response.coords.longitude
                    },
                    {
                        latitude: parseFloat(dest.userDetail.address[0].lat),
                        longitude: parseFloat(dest.userDetail.address[0].lng),
                    }
                ];
                this.setState({coordinates: coords});
            });
        }
    }

    onMapPress = e => {
        this.setState({
            coordinates: [
                ...this.state.coordinates,
                e.nativeEvent.coordinate,
            ],
        });
    };

    onReady = (result) => {
        this.mapView.fitToCoordinates(result.coordinates, {
            edgePadding: {
                right: (width / 10),
                bottom: (height / 10),
                left: (width / 10),
                top: (height / 10),
            },
        });
    };

    onLocationChange = (coords) => {
        console.log('onLocationChange------>>>', coords.nativeEvent.coordinate);
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <Maps
                    ref={c => this.mapView = c}
                    provider={PROVIDER_GOOGLE}
                    showsUserLocation={true}
                    showsMyLocationButton={true}
                    showsCompass={false}
                    // customMapStyle={MAP_JSON}
                    onPress={this.onMapPress}
                    style={styles.map}
                    onUserLocationChange={this.onLocationChange}
                    region={{
                        latitude: LATITUDE,
                        longitude: LONGITUDE,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }}
                >
                    {this.state.coordinates.map((coordinate, index) =>
                        <Maps.Marker key={`coordinate_${index}`} coordinate={coordinate} />
                    )}
                    {(this.state.coordinates.length >= 2) && (
                        <MapViewDirections
                            origin={this.state.coordinates[0]}
                            waypoints={this.state.coordinates.slice(1,-1)}
                            destination={this.state.coordinates[this.state.coordinates.length-1]}
                            apikey={GOOGLE_MAPS_API_KEY}
                            mode='DRIVING'
                            language='en'
                            strokeWidth={4}
                            strokeColor="blue"
                            optimizeWaypoints={true}
                            onStart={(params) => {
                                console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
                            }}
                            onReady={this.onReady}
                            onError={(errorMessage) => {
                                console.log('GOT AN ERROR---->', errorMessage);
                            }}
                            resetOnChange={false}
                        />
                    )}
                </Maps>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    map: {
       ...StyleSheet.absoluteFill
    },
});

const mapStateToProps = state => {
    return {
        userProfile: state.login.userProfile,
    };
};

export default connect(
    mapStateToProps,
    null,
)(MapView);
