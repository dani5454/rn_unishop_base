import React from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {fetchUserProfile} from '../redux/actions/user.action';
import firebase from 'react-native-firebase';
import {connect} from 'react-redux';

class Splash extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (firebase.auth().currentUser) {
      let uid = {uid: firebase.auth().currentUser.uid};
      this.props.fetchProfile({...uid});
    } else {
      setTimeout(() => {
        this.props.navigation.reset({
          index: 0,
          routes: [{name: 'OnBoard'}],
        });
      }, 5000);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('./../assets/images/splash_gif.gif')}
          style={styles.gifImage}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  gifImage: {
    height: null,
    width: null,
    flex: 1,
  },
});

const mapDispatchToProps = dispatch => {
  return {
    fetchProfile: uid => dispatch(fetchUserProfile(uid)),
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(Splash);
