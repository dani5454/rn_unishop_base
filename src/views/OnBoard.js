import React from 'react';
import {View, ImageBackground, TouchableOpacity, Text, StyleSheet} from 'react-native';

export default class OnBoard extends React.Component {

    render() {
        const {navigation} = this.props;
        return (
            <ImageBackground
                style={styles.container}
                source={require('./../assets/images/onBoard.jpg')}
            >
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => navigation.navigate('Login')}
                    >
                        <ImageBackground
                            source={require('./../assets/images/button.png')}
                            style={styles.button}
                        >
                            <Text style={{color: '#ffffff'}}>Iniciar sesión</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => navigation.navigate('Register')}
                    >
                        <ImageBackground
                            source={require('./../assets/images/button.png')}
                            style={styles.button}
                        >
                            <Text style={{color: '#ffffff'}}>Crear cuenta</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnContainer: {
        height: 150,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    button: {
        width: 230,
        height: 55,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
