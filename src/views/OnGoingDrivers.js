import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, SafeAreaView, FlatList} from 'react-native';
import firebaseModal from '../firebase/Firebase';

export default class OnGoingDrivers extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ListDrivers: [],
            fetching: false
        }
    }

    componentDidMount() {
        firebaseModal.fetchOnGoingOrder((resp) => {
            if(!resp.error) {
              this.setState({ListDrivers:resp.data});
            }
        });
    }

    renderDrivers = (item, index) => {
        return(
            <TouchableOpacity activeOpacity={0.8}
                              style={styles.listView}
                              onPress={() => this.props.navigation.navigate('MapView', {shopper: true,detail:item.data()})}
            >
                <Text style={{marginLeft:15}}>{index} </Text>
                <Text> Verificar el estado de  </Text>
                <Text style={{textDecorationLine: 'underline'}}>{item.data().userDetail.lastName}</Text>
            </TouchableOpacity>
        )
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={this.state.ListDrivers}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    renderItem={({item, index}) => this.renderDrivers(item, index)}
                    // refreshing={this.state.fetching}
                    // onRefresh={this.onRefresh}
                    keyExtractor={(item, index) => `message__${index}`}
                />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
   container: {
       flex:1
   },
    listView: {
        flexDirection:'row',
        height:50,
        alignItems: 'center',
    },
    separator: {
       borderBottomWidth: StyleSheet.hairlineWidth
    }
});
