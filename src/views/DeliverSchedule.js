import React from 'react';
import {
  View,
  Alert,
  Text,
  TouchableWithoutFeedback,
  StyleSheet,
  SafeAreaView, FlatList,
} from 'react-native';
import Fonts from '../utilMethods/FontFamily';
import {Width, Height} from '../utilMethods/Utils';
import {connect} from 'react-redux';
import firebaseModal from '../firebase/Firebase';
import DeliveryCard from '../components/DeliverCard';

class DeliverySchedule extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabNavigator: false,
      onWayOrders: [],
      completeOrders: [],
    };
  }

  componentDidMount() {
    this.props.navigation.addListener('focus', () => {
      if(!this.props.userProfile.shopper) {
        this.fetchOnGoingOrders('driver_on_way');
        this.fetchCompleteOrders();
      }  else {
        this.fetchOnGoingOrders('dispatched');
      }
    });
  }

  fetchOnGoingOrders = (status) => {
    firebaseModal.fetchOrders((resp) => {
      let onWayOrders = [];
      let status = this.props.userProfile.shopper ? 'dispatched' : 'driver_on_way';
      if(!resp.error) {
        resp.data.filter(item => {
          if (item.data().userDetail !== '') {
            if(this.props.userProfile.shopper) {
              if (item.data().status === 'dispatched' || item.data().status === 'driver_on_way') {
                onWayOrders.push(item.data());
              }
            } else {
              if (item.data().status === status) {
                onWayOrders.push(item.data());
              }
            }
          }
        });
        this.setState({onWayOrders,fetching: false});
      } else {
        this.setState({fetching: false})
      }
    });
  };

  fetchCompleteOrders = () => {
    // TODO : fetch driver complete orders
  };

  firstTab = () => {
    if (this.state.tabNavigator) {
      this.setState({tabNavigator: false});
    }
  };

  secondTab = () => {
    if (!this.state.tabNavigator) {
      this.setState({tabNavigator: true});
    }
  };

  renderOnWayOrders = (order, index) => {
    let name = order.userDetail.name;
    let address = order.userDetail.address[0].addressAndNumber;
    let userImage = order.userDetail.socialProfilePic;
    return <DeliveryCard type={'selection'}
                         index={order.orderId}
                         clientName={name}
                         Direction={address}
                         image={userImage}
    />
  };

  renderCompleteOrders = (order, index) => {

  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{justifyContent: 'center', flexDirection: 'row'}}>
          <TouchableWithoutFeedback onPress={() => this.firstTab()}>
            <View
              style={[
                styles.topLeftNavigator,
                styles.content,
                {
                  backgroundColor: !this.state.tabNavigator
                    ? '#6c9f27'
                    : '#ffffff',
                },
              ]}>
              <Text style={Fonts.gothamRounded_bold}>Despachada</Text>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => this.secondTab()}>
            <View
              style={[
                styles.topRightNavigator,
                styles.content,
                {
                  backgroundColor: this.state.tabNavigator
                    ? '#6c9f27'
                    : '#ffffff',
                },
              ]}>
              <Text style={Fonts.gothamRounded_bold}>Completada</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.container}>
          {!this.state.tabNavigator && (
              <FlatList
                  data={this.state.onWayOrders}
                  removeClippedSubviews={true}
                  initialNumToRender={10}
                  maxToRenderPerBatch={10}
                  legacyImplementation={true}
                  updateCellsBatchingPeriod={50}
                  ItemSeparatorComponent={() => <View style={styles.separator} />}
                  renderItem={({item, index}) => this.renderOnWayOrders(item, index)}
                  keyExtractor={(item, index) => `message_${index}`}
              />
          )}
          {this.state.tabNavigator && (
              <FlatList
                  data={this.state.completeOrders}
                  removeClippedSubviews={true}
                  initialNumToRender={10}
                  maxToRenderPerBatch={10}
                  legacyImplementation={true}
                  updateCellsBatchingPeriod={50}
                  ItemSeparatorComponent={() => <View style={styles.separator} />}
                  renderItem={({item, index}) =>
                      this.renderCompleteOrders(item, index)
                  }
                  keyExtractor={(item, index) => `message_${index}`}
              />
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topLeftNavigator: {
    borderWidth: 1,
    width: Width('30%'),
    height: Height('3%'),
    borderTopLeftRadius: Width('4%'),
    borderBottomLeftRadius: Width('4%'),
  },
  topRightNavigator: {
    borderWidth: 1,
    width: Width('30%'),
    height: Height('3%'),
    borderTopRightRadius: Width('4%'),
    borderBottomRightRadius: Width('4%'),
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = state => {
  return {
    userProfile: state.login.userProfile,
  };
};

export default connect(
    mapStateToProps,
    null,
)(DeliverySchedule);
