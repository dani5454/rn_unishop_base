import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
    Alert
} from 'react-native';
import {Height, Width} from '../utilMethods/Utils';
import Fonts from '../utilMethods/FontFamily';
import {connect} from 'react-redux';
import firebaseModal from '../firebase/Firebase';

class UpdateUserProfile extends React.Component {

    constructor(props) {
        super(props);

        this.props.navigation.setOptions({
            title: ''
        });

        this.state = {
            name: this.props.userProfile.name,
            address: this.props.userProfile.address,
            pegregal: this.props.userProfile.lastName,
            phoneNumber: this.props.userProfile.phone.toString(),
        }
    }

    updateMarket = () => {
        const {name,address,pegregal,phoneNumber} = this.state;

        if(name.length < 5) {
            Alert.alert('Error', 'la longitud del nombre es pequeña');
            return true;
        }
        if(address.length < 5) {
            Alert.alert('Error', 'la longitud de la dirección es pequeña');
            return true;
        }
        if(pegregal.length < 5) {
            Alert.alert('Error', 'pegregal la longitud es pequeña');
            return true;
        }
        if(phoneNumber.length < 7) {
            Alert.alert('Error', 'la longitud del número de teléfono es pequeña');
            return true;
        }

        let user = {
            name:name,
            address:address,
            pegregal:pegregal,
            phoneNumber:phoneNumber,
            email:this.props.userProfile.email,
            employeeId:this.props.userProfile.employeeId,
            shopper:this.props.userProfile.shopper,
        };

        firebaseModal.updateUserProfile(user, (resp) => {
            if(!resp.error) {
                Alert.alert('Éxito', 'Perfil actualizado con éxito');
            }
        });

    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>
                    <View style={styles.inputContainer}>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Nombre'}
                                value={this.state.name}
                                onChangeText={(name) => this.setState({name})}
                            />
                        </View>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Habla a'}
                                value={this.state.address}
                                onChangeText={(address) => this.setState({address})}
                            />
                        </View>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Col Pedregal'}
                                value={this.state.pegregal}
                                onChangeText={(pegregal) => this.setState({pegregal})}
                            />
                        </View>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                keyboardType={'numeric'}
                                placeholder={'Número de teléfono'}
                                value={this.state.phoneNumber}
                                onChangeText={(phoneNumber) => this.setState({phoneNumber})}
                            />
                        </View>
                    </View>
                    <View style={styles.bottomContainer}>
                        <TouchableWithoutFeedback
                            onPress={() => this.updateMarket()}
                        >
                            <View style={styles.readyBtn}>
                                <Text style={[Fonts.gothamRounded_bold, {color: '#6c9f27'}]}>
                                    LISTO
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    button: {
        height: Height('5'),
        width: Width('28'),
        borderRadius: Width('2'),
        marginTop: Height('2'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#6c9f27'
    },
    inputContainer: {
        alignItems: 'center'
    },
    input: {
        marginTop: Height('2'),
        width: Width('70'),
        height: Height('4'),
        borderBottomWidth: 2,
        borderColor: '#6c9f27'
    },
    bottomContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    readyBtn: {
        height: Height('5'),
        width: Width('85'),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderRadius: Width('8'),
        borderColor: '#6c9f27'
    }
});

const mapStateToProps = state => {
    return {
        userProfile: state.login.userProfile,
    };
};

export default connect(
    mapStateToProps,
    null,
)(UpdateUserProfile);
