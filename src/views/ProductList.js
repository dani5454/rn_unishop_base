import React from 'react';
import {FlatList, Image, Alert, TouchableOpacity, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import firebaseModal from '../firebase/Firebase';
import Fonts from '../utilMethods/FontFamily';
import firebase from 'react-native-firebase';

export default class ProductList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: []
        };

        this.props.navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity
                    activeOpacity={0.5}
                    style={{marginRight: 10}}
                    onPress={() => this.uploadProducts()}
                >
                    <Image source={require('./../assets/images/upload-arrow-512.png')}
                           style={{height:30, width: 30, tintColor: '#FFA500'}}
                           />
                </TouchableOpacity>
            ),
        });
    }

    uploadProducts = () => {
        const productsList = this.props.route.params.order;
        let driver = productsList.driver = '';
        productsList.shopper = firebase.auth().currentUser.uid;
        firebaseModal.updateOrderStatus(productsList, driver, (resp) => {
            if(!resp.error) {
                Alert.alert('Éxito','Lista actualizada correctamente');
            }
        });
    };

    componentDidMount() {
        let products = this.props.route.params.order.userOrder;
        this.setState({products})
    }

    onItemPressed = (item) => {
        const {products} = this.state;
        item.checked = !item.checked;
        this.setState({products});
    };

    renderProducts = (item, index) => {
        return(
        <View style={styles.content}>
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.checked, { backgroundColor: item.checked?'#FFA500':'#F0F0F0'}]}
                onPress={() => this.onItemPressed(item)}
            >
                <Image
                    source={require('./../assets/images/tick-512.png')}
                    style={{width: 15, height:15,tintColor:'#F0F0F0'}}
                />
            </TouchableOpacity>
            <Text style={[Fonts.gothamRounded_bold, styles.font]}>{item.name}</Text>
        </View>
        );
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={this.state.products}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    renderItem={({item, index}) => this.renderProducts(item, index)}
                    keyExtractor={(item, index) => `message ${index}`}
                />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        marginTop: 50,
        marginLeft: 24
    },
    separator: {
        height: 10
    },
    content: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    checked: {
        width: 20,
        height: 20,
        borderRadius: 10,
        borderWidth:1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    font: {
        marginLeft: 15,
        marginTop: 4
    }
});
