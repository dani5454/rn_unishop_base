import React from 'react';
import {
  View,
  ImageBackground,
  Alert,
  Text,
  StyleSheet,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {StackActions} from '@react-navigation/native';
import {Width, Height} from './../utilMethods/Utils';
import {userLogin} from '../redux/actions/user.action';
import Fonts from '../utilMethods/FontFamily';
import {connect} from 'react-redux';
import Loader from '../components/Loader';
import Header from '../components/Header';
import firebaseModal from '../firebase/Firebase';

let isEmail = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
    };
  }

  signIn = () => {
    const {email, password} = this.state;
    if (email.length > 0) {
      if (isEmail.test(email)) {
        if (password.length >= 9) {
          this.validateUser();
        } else {
          Alert.alert(
            'Error',
            'La longitud mínima de la contraseña es de 9 caracteres.',
          );
        }
      } else {
        Alert.alert(
          'Error de sintaxis',
          'Formato de correo electrónico incorrecto',
        );
      }
    } else {
      Alert.alert('Error', 'El correo electrónico no puede estar vacío');
    }
  };

  validateUser = () => {
    this.props.userLogin({...this.state});
    // firebaseModal.signIn(email,password,(resp) => {
    //   if(!resp.error) {
    //     firebaseModal.fetchMarketProfile(async (profile) => {
    //       if(!profile.error) {
    //         if (profile.data.driver) {
    //
    //         } else {
    //
    //         }
    //       } else {
    //        await firebaseModal.signOut();
    //         Alert.alert('Error del Servidor','Error de servidor interno.Inténtalo de nuevo');
    //       }
    //     });
    //   } else {
    //     Alert.alert('Error del Servidor', resp.errorMessage);
    //   }
    // })
  };

  render() {
    return (
      <ImageBackground
        source={require('./../assets/images/Login.jpg')}
        style={styles.container}>
        <SafeAreaView style={styles.container}>
          {Loader.renderLoading(this.props.isLoader)}
          <Header text={'REGISTRATE'} />
          <View style={styles.inputContainer}>
            <View style={styles.inputField}>
              <TextInput
                style={[Fonts.gothamRounded_bold, {flex: 1}]}
                placeholder={'Correo electrónico'}
                autoCapitalize="none"
                keyboardType={'email-address'}
                autoCorrect={false}
                value={this.state.email}
                onChangeText={email => this.setState({email})}
              />
            </View>
            <View style={[styles.inputField, {marginTop: Height('3%')}]}>
              <TextInput
                style={[Fonts.gothamRounded_bold, {flex: 1}]}
                placeholder={'Contraseña'}
                autoCapitalize="none"
                secureTextEntry={true}
                value={this.state.password}
                onChangeText={password => this.setState({password})}
              />
            </View>
            <View style={{flex: 1}}>
              <TouchableOpacity
                style={styles.loginBtn}
                onPress={() => this.signIn()}>
                <Text style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>
                  INICIAR SESION
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputContainer: {
    flex: 1,
    marginTop: Height('3%'),
  },
  inputField: {
    height: Height('4.5%'),
    borderBottomWidth: 2,
    borderBottomColor: '#D64B12',
    marginLeft: Width('4%'),
    marginRight: Width('4%'),
  },
  loginBtn: {
    backgroundColor: '#d64b12',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Height('3%'),
    borderRadius: Width('6.5%'),
    marginLeft: Width('10%'),
    height: Height('5%'),
    width: Width('40%'),
  },
});

const mapDispatchToProps = dispatch => {
  return {
    userLogin: userData => dispatch(userLogin(userData)),
  };
};

const mapStateToProps = state => {
  return {
    isLoader: state.login.isLoader,
    userProfile: state.login.userProfile,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
