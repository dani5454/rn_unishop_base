import React from 'react';
import {
  View,
  ImageBackground,
  Text,
  Alert,
  StyleSheet,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {registerUser} from '../redux/actions/user.action';
import {Width, Height} from './../utilMethods/Utils';
import Fonts from '../utilMethods/FontFamily';
import Loader from '../components/Loader';
import Header from '../components/Header';
import {connect} from 'react-redux';
import { Dropdown } from 'react-native-material-dropdown';

const screenWidth = Dimensions.get('window').width;

let isEmail = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;

class Register extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // uploadImage: false,
      // uploadLogo: false,
      firstName: '',
      lastName: '',
      phone: '',
      email: '',
      driverId: '',
      type: '',
      // image: '',
      // logo: '',
      address: '',
      password: '',
      confirmPassword: '',
    };
  }

  signUp = () => {
    const {
      firstName,
      lastName,
      email,
      address,
      password,
      phone,
      driverId,
      confirmPassword,
      type
    } = this.state;
    if (!isEmail.test(email)) {
      Alert.alert('Error', 'Formato de correo electrónico incorrecto');
      return false;
    }
    if (firstName.length < 1) {
      Alert.alert('Error', 'Por favor ingrese el nombre');
      return false;
    }
    if (lastName.length < 1) {
      Alert.alert('Error', 'Por favor ingrese el nombre de usuario');
      return false;
    }
    if (driverId.length < 4) {
      Alert.alert(
        'Error',
        'ingrese al menos 4 dígitos de identificación de empleado',
      );
      return false;
    }
    if (address.length < 1) {
      Alert.alert('Error', 'Por favor ingrese la dirección');
      return false;
    }
    if (phone.length < 1) {
      Alert.alert('Error', 'Por favor ingrese el teléfono');
      return false;
    }
    if (password.length < 9) {
      Alert.alert(
        'Error',
        'La longitud mínima de la contraseña es de 9 caracteres',
      );
      return false;
    }
    if (password !== confirmPassword) {
      Alert.alert('Error', 'confirmar contraseña y contraseña no coinciden');
      return false;
    }
    if (type.length < 1) {
      Alert.alert('Error', 'Por favor seleccione tipo');
      return false;
    }
    this.props.registerUser({...this.state});
  };

  // uploadImage = () => {};

  // uploadLogo = () => {};

  render() {
    let data = [{
      value: 'Conductor',
    }, {
      value: 'Comprador',
    }];
    return (
      <ImageBackground
        source={require('./../assets/images/register.jpg')}
        style={styles.container}>
        <SafeAreaView style={styles.container}>
          {Loader.renderLoading(this.props.isLoader)}
          <Header text={'INICIAR SESION'} />
          <View style={styles.inputContainer}>
            <View style={styles.rowContainer}>
              <View style={[styles.inputField, {width: screenWidth / 2 - 20}]}>
                <TextInput
                  style={[Fonts.gothamRounded_bold, {flex: 1}]}
                  placeholder={'nombre'}
                  autoCapitalize="none"
                  autoCorrect={false}
                  value={this.state.firstName}
                  onChangeText={firstName => this.setState({firstName})}
                />
              </View>
              <View style={[styles.inputField, {width: screenWidth / 2}]}>
                <TextInput
                  style={[Fonts.gothamRounded_bold, {flex: 1}]}
                  placeholder={'nombre de usuario'}
                  autoCapitalize="none"
                  autoCorrect={false}
                  value={this.state.lastName}
                  onChangeText={lastName => this.setState({lastName})}
                />
              </View>
            </View>
            <View style={[styles.inputField, {marginTop: Height('3%')}]}>
              <TextInput
                style={[Fonts.gothamRounded_bold, {flex: 1}]}
                placeholder={'Correo electrónico'}
                autoCapitalize="none"
                keyboardType={'email-address'}
                autoCorrect={false}
                value={this.state.email}
                onChangeText={email => this.setState({email})}
              />
            </View>
            <View
              style={{
                height: Height('4.5%'),
                flexDirection: 'row',
                marginTop: Height('3%'),
                marginLeft: Width('4%'),
                alignItems: 'center',
              }}>
              <Text style={Fonts.gothamRounded_bold}>Nui - </Text>
              <View
                style={{
                  borderBottomWidth: 2,
                  borderBottomColor: '#D64B12',
                  marginRight: Width('4%'),
                  flex: 1,
                }}>
                <TextInput
                  style={[Fonts.gothamRounded_bold, {flex: 1}]}
                  placeholder={'ID de empleado'}
                  keyboardType={'numeric'}
                  maxLength={4}
                  autoCapitalize="none"
                  autoCorrect={false}
                  value={this.state.driverId}
                  onChangeText={driverId => this.setState({driverId})}
                />
              </View>
            </View>
            <View style={[styles.inputField, {marginTop: Height('3%')}]}>
              <TextInput
                style={[Fonts.gothamRounded_bold, {flex: 1}]}
                placeholder={'dirección'}
                autoCapitalize="none"
                autoCorrect={false}
                value={this.state.address}
                onChangeText={address => this.setState({address})}
              />
            </View>
            <View style={[styles.inputField, {marginTop: Height('3%')}]}>
              <TextInput
                style={[Fonts.gothamRounded_bold, {flex: 1}]}
                placeholder={'Teléfono'}
                autoCapitalize="none"
                autoCorrect={false}
                value={this.state.phone}
                onChangeText={phone => this.setState({phone})}
              />
            </View>
            <View style={styles.modalDropDown}>
            <Dropdown
                label='Tipo'
                inputContainerStyle={{borderBottomWidth: 2,borderBottomColor: '#d64b12'}}
                data={data}
                onChangeText={val => this.setState({type:val})}
            />
            </View>
            <View style={[styles.inputField, {marginTop: Height('5%')}]}>
              <TextInput
                style={[Fonts.gothamRounded_bold, {flex: 1}]}
                placeholder={'Contraseña'}
                autoCapitalize="none"
                secureTextEntry={true}
                value={this.state.password}
                onChangeText={password => this.setState({password})}
              />
            </View>
            <View style={[styles.inputField, {marginTop: Height('3%')}]}>
              <TextInput
                style={[Fonts.gothamRounded_bold, {flex: 1}]}
                placeholder={'Confirmar contraseña'}
                autoCapitalize="none"
                secureTextEntry={true}
                value={this.state.confirmPassword}
                onChangeText={confirmPassword =>
                  this.setState({confirmPassword})
                }
              />
            </View>
            <View style={styles.container}>
              {/*<View style={styles.uploadImage}>
                <TouchableWithoutFeedback onPress={() => this.uploadImage()}>
                  <View style={styles.button}>
                    <Text
                      style={[
                        Fonts.gothamRounded_bold,
                        styles.uploadImageText,
                      ]}>
                      SELECCIONAR IMAGEN
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.uploadLogo()}>
                  <View style={styles.button}>
                    <Text
                      style={[
                        Fonts.gothamRounded_bold,
                        styles.uploadImageText,
                      ]}>
                      SELECCIONAR LOGO
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>*/}
              <TouchableOpacity
                onPress={() => this.signUp()}
                style={styles.registerBtn}>
                <Text style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>
                  CREAR CUENTA
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputContainer: {
    flex: 1,
    flexDirection: 'column',
    marginTop: Height('3%'),
  },
  rowContainer: {
    flexDirection: 'row',
    marginRight: Width('4%'),
    overflow: 'hidden',
  },
  inputField: {
    height: Height('4.5%'),
    borderBottomWidth: 2,
    borderBottomColor: '#D64B12',
    marginLeft: Width('4%'),
    marginRight: Width('4%'),
  },
  modalDropDown: {
    height: Height('4.5%'),
    marginLeft: Width('4%'),
    marginRight: Width('4%'),
  },
  registerBtn: {
    backgroundColor: '#d64b12',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Height('3%'),
    borderRadius: Width('6.5%'),
    marginLeft: Width('10%'),
    height: Height('5%'),
    width: Width('40%'),
  },
  uploadImage: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    marginTop: Height('3'),
  },
  uploadImageText: {
    color: '#FFF',
    fontSize: Width('3'),
  },
  button: {
    width: Width('45'),
    height: Height('4'),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: Width('3'),
    backgroundColor: '#6c9f27',
  },
});

const mapDispatchToProps = dispatch => {
  return {
    registerUser: userData => dispatch(registerUser(userData)),
  };
};

const mapStateToProps = state => {
  return {
    isLoader: state.login.isLoader,
    userProfile: state.login.userProfile,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register);
