import React from 'react';
import {Alert, FlatList, SafeAreaView, StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native';
import {Height, Width} from '../utilMethods/Utils';
import Fonts from '../utilMethods/FontFamily';
import firebaseModal from '../firebase/Firebase';
import firebase from 'react-native-firebase';
import Card from '../components/Card';
import {connect} from 'react-redux';

class Store extends React.Component {

  attempt = 0;

  constructor(props) {
    super(props);

    this.state = {
      tabNavigator: false,
      dispatch: false,
      acceptedOrder: [],
      dispatchOrder: [],
      fetching: false
    };
  }

  // TODO : STATUS => 'driver_accepted', 'driver_rejected', 'driver_on_way', 'driver_completed'

  componentDidMount() {
    this.setState({fetching: true});
    this.props.navigation.addListener('focus', () => {
      if (!this.props.userProfile.shopper) {
        this.fetchUserData('dispatched', 'driver_accepted');
      } else {
        this.fetchUserData('requested', 'accepted');
      }
    })
  }

  fetchUserData = async (requested, dispatch) => {
    firebaseModal.fetchOrders((resp) => {
      let acceptedOrder = [], dispatchOrder = [];
      if(!resp.error) {
        resp.data.filter(item=> {
          if (item.data().userDetail !== '') {
            if (item.data().status === requested) {
              acceptedOrder.push(item.data());
            } else if (item.data().status === dispatch) {
              dispatchOrder.push(item.data());
            }
          }
        });
        this.setState({acceptedOrder, dispatchOrder,fetching: false});
      } else {
        this.setState({fetching: false})
      }
    });
  };

  firstTab = () => {
    if (this.state.tabNavigator) {
      this.setState({tabNavigator: false});
    }
  };

  secondTab = () => {
    if (!this.state.tabNavigator) {
      this.setState({tabNavigator: true});
    }
  };

  renderOrders = (item, index) => {
    let name = item.userDetail.name;
    let address = item.userDetail.address[0].addressAndNumber;
    let userImage = item.userDetail.socialProfilePic;
    let dispatch = this.props.userProfile.shopper ? 'requested' : 'dispatched';
    if (item.status === dispatch) {
      return (
        <Card
          type={'selection'}
          index={item.orderId}
          clientName={name}
          Direction={address}
          image={userImage}
          accept={this.accept.bind(this, item,index)}
          reject={this.reject.bind(this, item,index)}
          cardOnPress={this.cardOnPress.bind(this, item,index)}
        />
      );
    }
  };

  cardOnPress = (order, index) => {
    if(this.props.userProfile.shopper) {
      this.props.navigation.navigate('ProductList', {order: order});
    }
  };

  accept = async (order, index) => {
    let status = false;
    const {acceptedOrder} = this.state;
    let dispatch = this.props.userProfile.shopper ? 'requested' : 'dispatched';
    firebaseModal.fetchOrders((resp) => {
      if(!resp.error) {
        resp.data.map(item => {
          if(parseInt(item.data().orderId) === parseInt(order.orderId)) {
            if(item.data().status === dispatch) {
              status = true;
              return this.acceptAction(order, index);
            }
          }
        });
        if(!status) {
          acceptedOrder.splice(index, 1);
          let error = this.props.userProfile.shopper ?
              'orden ya ha sido aceptada por otro comprador' :
          'el pedido ya ha sido aceptado por otro conductor';
          Alert.alert('Error', error);
          this.setState({acceptedOrder});
        }
      }
    });
  };

  acceptAction = (order, index) => {
    let arr = [];let driver = '';
    order.status = this.props.userProfile.shopper ? 'accepted' : 'driver_accepted';
    if(this.props.userProfile.shopper) {
      order.shopper = firebase.auth().currentUser.uid;
      driver = '';
    } else {
      driver = firebase.auth().currentUser.uid;
    }
    this.setState({fetching: true});
    const {dispatchOrder,acceptedOrder} = this.state;
    firebaseModal.updateOrderStatus(order, driver, (resp) => {
      if(!resp.error) {
        if(dispatchOrder.length === 0) {
          arr.push(order);
          acceptedOrder.splice(index, 1);
          this.setState({dispatchOrder: arr,acceptedOrder,fetching: false})
        } else {
          acceptedOrder.splice(index, 1);
          for(let i=0; i<dispatchOrder.length; i++) {
            arr.push(dispatchOrder[i]);
          }
          arr.push(order);
          this.setState({dispatchOrder: arr,acceptedOrder,fetching: false})
        }
      }
    });
  };

  reject = async (order, index) => {
    const {acceptedOrder} = this.state;
    let driver = '';
    order.status = this.props.userProfile.shopper ? 'rejected' : 'driver_rejected';
    if(this.props.userProfile.shopper) {
      order.shopper = firebase.auth().currentUser.uid;
      driver = '';
    } else {
      driver = firebase.auth().currentUser.uid;
    }
    order.driver = firebase.auth().currentUser.uid;
    acceptedOrder.splice(index,1);
    firebaseModal.updateOrderStatus(order, driver, (resp) => {
      if(!resp.error) {
        this.setState({acceptedOrder});
      }
    })
  };

  renderDispatchOrders = (order, count) => {
    let name = order.userDetail.name + ' ' + order.userDetail.lastName;
    let address = order.userDetail.address[0].addressAndNumber;
    let userImage = order.userDetail.socialProfilePic;
    return (
      <Card
        type={'dispatch'}
        index={order.orderId}
        clientName={name}
        Direction={address}
        image={userImage}
        dispatch={this.dispatch.bind(this, order, count)}
        cardOnPress={this.pressed.bind(this, order,count)}
      />
    );
  };

  pressed = (order, index) => {

  };

  dispatch = (order, index) => {
    const {dispatchOrder} = this.state;
    let driver = '';
    order.status = this.props.userProfile.shopper ? 'dispatched' : 'driver_on_way';
    if(this.props.userProfile.shopper) {
      order.shopper = firebase.auth().currentUser.uid;
      driver = '';
    } else {
      driver = firebase.auth().currentUser.uid;
    }
    order.driver = firebase.auth().currentUser.uid;
    firebaseModal.updateOrderStatus(order, driver,(resp) => {
      if(!resp.error) {
        dispatchOrder.splice(index,1);
        this.setState({dispatchOrder})
      }
    })
  };

  onRefresh = () => {
    this.setState({fetching: true});
    if(!this.props.userProfile.shopper) {
      this.fetchUserData('dispatched','driver_accepted');
    } else {
      this.fetchUserData('requested','dispatched');
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{justifyContent: 'center', flexDirection: 'row'}}>
          <TouchableWithoutFeedback onPress={() => this.firstTab()}>
            <View
              style={[
                styles.topLeftNavigator,
                styles.content,
                {
                  backgroundColor: !this.state.tabNavigator
                    ? '#6c9f27'
                    : '#ffffff',
                },
              ]}>
              <Text style={Fonts.gothamRounded_bold}>Nueva</Text>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => this.secondTab()}>
            <View
              style={[
                styles.topRightNavigator,
                styles.content,
                {
                  backgroundColor: this.state.tabNavigator
                    ? '#6c9f27'
                    : '#ffffff',
                },
              ]}>
              <Text style={Fonts.gothamRounded_bold}>Aceptada</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.container}>
          {!this.state.tabNavigator && (
              <FlatList
                  data={this.state.acceptedOrder}
                  ItemSeparatorComponent={() => <View style={styles.separator} />}
                  renderItem={({item, index}) => this.renderOrders(item, index)}
                  refreshing={this.state.fetching}
                  onRefresh={() => this.onRefresh()}
                  keyExtractor={(item, index) => `message ${index}`}
              />
          )}
          {this.state.tabNavigator && (
              <FlatList
                  data={this.state.dispatchOrder}
                  ItemSeparatorComponent={() => <View style={styles.separator} />}
                  renderItem={({item, index}) =>
                      this.renderDispatchOrders(item, index)
                  }
                  keyExtractor={(item, index) => `message ${index}`}
              />
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topLeftNavigator: {
    borderWidth: 1,
    width: Width('30%'),
    height: Height('3%'),
    borderTopLeftRadius: Width('4%'),
    borderBottomLeftRadius: Width('4%'),
  },
  topRightNavigator: {
    borderWidth: 1,
    width: Width('30%'),
    height: Height('3%'),
    borderTopRightRadius: Width('4%'),
    borderBottomRightRadius: Width('4%'),
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = state => {
  return {
    userProfile: state.login.userProfile,
  };
};

export default connect(
    mapStateToProps,
    null,
)(Store);
